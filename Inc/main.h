/* USER CODE BEGIN Header */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ID_BIT_0_Pin GPIO_PIN_0
#define ID_BIT_0_GPIO_Port GPIOA
#define ID_BIT_1_Pin GPIO_PIN_1
#define ID_BIT_1_GPIO_Port GPIOA
#define ID_BIT_2_Pin GPIO_PIN_2
#define ID_BIT_2_GPIO_Port GPIOA
#define ID_BIT_3_Pin GPIO_PIN_3
#define ID_BIT_3_GPIO_Port GPIOA
#define ID_BIT_4_Pin GPIO_PIN_4
#define ID_BIT_4_GPIO_Port GPIOA
#define SPI_CS_Pin GPIO_PIN_1
#define SPI_CS_GPIO_Port GPIOB
#define IR_OUT_Pin GPIO_PIN_9
#define IR_OUT_GPIO_Port GPIOA
#define ID_BIT_5_Pin GPIO_PIN_10
#define ID_BIT_5_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
