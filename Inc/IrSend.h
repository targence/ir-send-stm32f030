#include "main.h"

extern TIM_HandleTypeDef htim1;
#define SEND_NEC 1

void custom_delay_usec(unsigned long uSecs);
void mark(unsigned int usec);
void space(unsigned int usec);
void sendRaw(unsigned int buf[], unsigned int len, uint8_t hz);

#if SEND_NEC
	void sendNEC(unsigned long data, int nbits);
#endif

void delayUs(unsigned long micros);
