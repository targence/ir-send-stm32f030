#include <IrSend.h>

void sendRaw(unsigned int buf[], unsigned int len, uint8_t hz)
{
	for(uint16_t i = 0; i < len; i++)
	{
		if(i & 1) space(buf[i]);
		else mark(buf[i]);
	}

	space(0);
}

void mark(unsigned int time)
{
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
	if (time > 0) delayUs(time);
}

void space(unsigned int time)
{
	HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_2);
	if(time > 0) delayUs(time);
}

unsigned long getUs(void)
{
  uint32_t usTicks = HAL_RCC_GetSysClockFreq() / 1000000;
  register uint32_t ms, cycle_cnt;
  do
  {
    ms = HAL_GetTick();
    cycle_cnt = SysTick->VAL;
  } while (ms != HAL_GetTick());

  return (ms * 1000) + (usTicks * 1000 - cycle_cnt) / usTicks;
}

void delayUs(unsigned long micros)
{
  uint32_t start = getUs();
  uint32_t correction = 0;
  while (getUs() - start < (uint32_t)micros - correction) {
	  asm("NOP");
  }
}
